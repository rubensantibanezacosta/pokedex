import React, {useContext} from "react";
import {AuthContext} from "../context/AuthContext";

export const useAuth:any = () => {
  return useContext(AuthContext);
}
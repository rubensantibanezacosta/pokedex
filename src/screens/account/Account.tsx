import React from 'react';
import { View, Text } from 'react-native';
import { useAuth } from '../../hooks/useAuth';
import { userDetails } from '../../utils/UserDb';


import { LoginForm } from './auth/loginForm/LoginForm';
import { UserData } from './auth/userData/UserData';

export const Account: React.FC<any> = (props) => {
    const {auth} = useAuth();

    
    return (
        <View>
            {auth? <UserData/>:<LoginForm>Formulario de login</LoginForm>}
        </View>
    );
}


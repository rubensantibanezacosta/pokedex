import React, {useState} from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { user, userDetails } from "../../../../utils/UserDb";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Button,
  Keyboard,
  ToastAndroid,
} from "react-native";
import { useAuth } from "../../../../hooks/useAuth";

export const LoginForm: React.FC<any> = (props) => {

  const { login, logout, userDetail } = useAuth();



  const formik: any = useFormik({
    initialValues: initialValues(),
    validationSchema: validationSchema(),
    validateOnChange: false,
    validateOnMount: false,
    onSubmit: (values) => {
      const { username, password } = values;
      if (username !== user.username || password !== user.password) {
        ToastAndroid.show(
          "Usuario o contraseña incorrectos",
          ToastAndroid.SHORT
        );
      }else{
        ToastAndroid.show(
            "Iniciando sesión",
            ToastAndroid.SHORT
          );
          login(userDetails);
          
      }
    },
  });

  return (
    <View>
      <Text style={styles.title}>Iniciar sesión</Text>
      <TextInput
        placeholder="Nombre de usuario"
        style={styles.input}
        autoCapitalize="none"
        value={formik.values.username}
        onChangeText={(text) => formik.setFieldValue("username", text)}
      />
      <TextInput
        placeholder="Contraseña"
        style={styles.input}
        secureTextEntry
        autoCapitalize="none"
        value={formik.values.password}
        onChangeText={(text) => formik.setFieldValue("password", text)}
      />
      <Button
        title="Iniciar sesión"
        onPress={() => {
          formik.handleSubmit(), Keyboard.dismiss();
        }}
      />
      <>
        {formik.errors.username?ToastAndroid.show(formik.errors.username, ToastAndroid.SHORT): null}
      </>
      <>
        {formik.errors.password?ToastAndroid.show(formik.errors.password, ToastAndroid.SHORT): null}
      </>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 28,
    fontWeight: "bold",
    marginTop: 40,
    marginBotton: 50,
    textAlign: "center",
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 8,
    borderRadius: 10,
    borderColor: "#ccc",
  },
  inputOnFocus: {
    borderColor: "#000",
  },
  error: {
    color: "red",
    textAlign: "center",
    marginTop: 20,
  },
});

function initialValues() {
  return {
    username: "",
    password: "",
  };
}

function validationSchema() {
  return Yup.object({
    username: Yup.string().required("El nombre de usuario es obligatorio"),
    password: Yup.string()
      .required("La contraseña es obligatoria")
      .min(6, "La contraseña debe tener al menos 6 caracteres")
      .max(30, "La contraseña no puede tener más de 30 caracteres"),
  });
}

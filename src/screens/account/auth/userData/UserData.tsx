import React, { useState, useCallback } from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import { useAuth } from "../../../../hooks/useAuth";
import { size } from "lodash";
import { useFocusEffect } from "@react-navigation/native";
import { getFavourites } from "../../../../clients/local/localStorage";

export const UserData: React.FC<any> = (props) => {
  const { auth, logout } = useAuth();

  const [total, setTotal] = useState(0);

  useFocusEffect(
    useCallback(() => {
      (async () => {
        const favourites = await getFavourites();
        setTotal(size(favourites));
      })();
    }, [])
  );

  return (
    <View style={styles.content}>
      <View style={styles.titleBlock}>
        <Text style={styles.title}>Bienvenido, </Text>
        <Text style={styles.title}>
          {auth.firstName} {auth.lastName}
        </Text>
      </View>
      <View style={styles.dataContent}>
        <ItemMenu title="Nombre" text={auth.firstName + " " + auth.lastName} />
        <ItemMenu title="Usuario" text={auth.username} />
        <ItemMenu title="Correo" text={auth.email} />
        <ItemMenu title="Favoritos" text={(total || 0) + " pokemons"} />
      </View>
      <View style={styles.logoutButton}>
        <Button title="Cerrar sesión" onPress={logout} color="red" />
      </View>
    </View>
  );
};

const ItemMenu: React.FC<any> = (props) => {
  const { title, text } = props;
  return (
    <View style={styles.itemMenu}>
      <Text style={styles.itemMenuTitle}>{title}:</Text>
      <Text>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    marginHorizontal: 20,
    marginTop: 20,
  },
  titleBlock: {
    margin: 30,
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
  },
  dataContent: {
    marginBottom: 20,
  },
  itemMenu: {
    flexDirection: "row",
    paddingVertical: 20,
    borderBottomWidth: 2,
    borderBottomColor: "#ccc",
  },
  itemMenuTitle: {
    fontWeight: "bold",
    paddingRight: 10,
    width: 120,
  },
  logoutButton: {
    paddingTop: 20,
  },
});

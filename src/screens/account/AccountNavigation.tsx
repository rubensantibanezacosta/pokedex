import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Account } from "./Account";

const Stack = createNativeStackNavigator();

export const AccountNavigation: React.FC<any> = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Account"
        component={Account}
        options={{
          title: "Mi cuenta",
        }}
      />
    </Stack.Navigator>
  );
};

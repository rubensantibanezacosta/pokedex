import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Favourite } from "./Favourite";
import { Pokemon } from "../pokeDex/pokemon/Pokemon";

const Stack = createNativeStackNavigator();

export const FavouriteNavigation: React.FC<any> = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Favourite"
        component={Favourite}
        options={{
          title: "Favoritos",
        }}
      />
      <Stack.Screen
        name="Pokemon"
        component={Pokemon}
        options={{
          title: "",
          headerTransparent: true,
        }}
      />
    </Stack.Navigator>
  );
};

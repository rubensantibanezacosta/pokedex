import { capitalize } from "lodash";
import React, { useEffect, useState, useCallback } from "react";
import { SafeAreaView, Text, Button } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { PokemonClient } from "../../clients/api/pokemonClient";
import {
  getFavourites,
  removeFromFavourites,
} from "../../clients/local/localStorage";
import { useAuth } from "../../hooks/useAuth";
import { PokemonList } from "../pokeDex/pokemonList/PokemonList";

export const Favourite: React.FC<any> = (props) => {
  const pokemonClient = PokemonClient.getInstance();

  const nullAny: any = null;
  const { auth } = useAuth();
  const [favorites, setFavorites] = useState(nullAny);

  useFocusEffect(
    useCallback(() => {
      if (auth) {
        (async () => {
          const res: any = await getFavourites();
          const pokemonsArray = [];

          for await (const pokemon of res) {
            console.log("reading pokemon", pokemon);
            const pokemonDetails = await pokemonClient.getPokemonDetail(
              pokemon
            );
            console.log(pokemonDetails, "pokemonDetails");
            pokemonsArray.push({
              id: pokemonDetails.id,
              name: capitalize(pokemonDetails.name),
              type: pokemonDetails.types[0].type.name,
              order: pokemonDetails.order,
              image:
                pokemonDetails.sprites.other["official-artwork"].front_default,
            });
          }
          setFavorites(pokemonsArray);
          console.log(pokemonsArray);
        })();
      }
    }, [auth])
  );

  return !auth ? (
    <Text>Usuario no logueado</Text>
  ) : (
    <SafeAreaView>
      <PokemonList pokemons={favorites} />
    </SafeAreaView>
  );
};

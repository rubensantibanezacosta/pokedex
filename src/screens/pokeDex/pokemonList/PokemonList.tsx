import React from "react";
import {
  StyleSheet,
  SafeAreaView,
  FlatList,
  ActivityIndicator,
  Platform
} from "react-native";
import { PokemonCard } from "./pokemonCard/PokemonCard";

export const PokemonList: React.FC<any> = (props) => {
  const { pokemons, loadPokemons, isNext } = props;


  const loadMore = () => {
    loadPokemons();
  };

  return (
    <SafeAreaView>
      <FlatList
        data={pokemons}
        numColumns={2}
        showsVerticalScrollIndicator={false}
        keyExtractor={(pokemon) => String(pokemon.id)}
        renderItem={({ item }) => <PokemonCard pokemon={item} />}
        contentContainerStyle={style.flatListContentContainerStyle}
        onEndReached={isNext && loadMore}
        onEndReachedThreshold={0.1}
        ListFooterComponent={() =>
          isNext && (
            <ActivityIndicator
              size="large"
              color="#aeaeae" 
              style={style.activityIndicator}
            />
          )
        }
      />
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  flatListContentContainerStyle: {
    paddingHorizontal: 10,
    paddingBottom: 30,
    paddingTop: Platform.OS === "android"? 60 : 30,
  },
  activityIndicator: {
    marginTop: 10,
    marginBottom: 40,
  },
});

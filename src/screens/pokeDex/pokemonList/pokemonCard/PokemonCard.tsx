import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { getColorByPokemonType } from "../../../../utils/pokemonColors";

export const PokemonCard: React.FC<any> = (props) => {
  const { pokemon } = props;
  const navigation:any = useNavigation();
  const pokemonColor = getColorByPokemonType(pokemon.type);

  const bgStyles = {
    backgroundColor: pokemonColor,
    ...styles.bgStyles,
  };

  const goToPokemon = () => {
    navigation.navigate("Pokemon", {id:pokemon.id} );
    //  props.navigation.navigate('Pokemon', { pokemon });
  };

  return (
    <TouchableWithoutFeedback onPress={() => goToPokemon()}>
      <View style={styles.card}>
        <View style={styles.spacing}>
          <View style={bgStyles}>
            <Text style={styles.number}>
              #{`${pokemon.order}`.padStart(3, "0")}
            </Text>
            <Text style={styles.name}>{pokemon.name}</Text>
            <Image source={{ uri: pokemon.image }} style={styles.image} />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  card: {
    flex: 1,
    height: 40,
    marginTop: 60,
    marginBottom: 60,
  },
  spacing: {
    flex: 1,
    padding: 5,
  },

  image: {
    position: "absolute",
    bottom: 2,
    right: 15,
    width: 130,
    height: 130,
  },
  bgStyles: {
    flex: 1,
    borderRadius: 3,
    paddingLeft: 10,
  },
  number: {
    position: "absolute",
    top: 6,
    right: 10,
    color: "white",

    fontSize: 11,
    zIndex: 1,
  },
  name: {
    color: "white",
    fontWeight: "bold",
    fontSize: 15,
    paddingTop: 2,    zIndex: 1,
    textShadowColor: "#00000",
    textShadowOffset: { width: 5, height: 4 },
  },
});

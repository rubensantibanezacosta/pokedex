import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { PokeDex } from "./PokeDex";
import { Pokemon } from "./pokemon/Pokemon";

const Stack = createNativeStackNavigator();

export const PokeDexNavigation: React.FC = () => {

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="PokeDex"
        component={PokeDex}
        options={{
          title: "",
          headerShown: false,
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Pokemon"
        component={Pokemon}
        options={{
          title: "",
          headerTransparent: true,
        }}
      />
    </Stack.Navigator>
  );
};

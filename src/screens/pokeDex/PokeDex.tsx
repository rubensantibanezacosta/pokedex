import React, { useState, useEffect } from "react";
import { SafeAreaView, Text } from "react-native";
import { PokemonClient } from "../../clients/api/pokemonClient";
import { PokemonList } from "./pokemonList/PokemonList";
import { capitalize } from "lodash";

export const PokeDex: React.FC<any> = (props) => {
  const pokemonClient = PokemonClient.getInstance();
  let initialDetails: any[] = [];
  const [pokemons, setPokemons] = useState(initialDetails);
  const [nextUrl, setNextUrl] = useState("");

  useEffect(() => {
    (async () => {
      await loadPokemons();
    })();
  }, []);

  const loadPokemons: any = async () => {
    const res = await pokemonClient.getPokemons(nextUrl);
    setNextUrl(res.next);
    const pokemonsArray = [];

    for await (const pokemon of res.results) {
      const pokemonDetails = await pokemonClient.getPokemonDetailByUrl(
        pokemon.url
      );

      pokemonsArray.push({
        id: pokemonDetails.id,
        name: capitalize(pokemonDetails.name),
        type: pokemonDetails.types[0].type.name,
        order: pokemonDetails.order,
        image: pokemonDetails.sprites.other["official-artwork"].front_default,
      });
    }

    setPokemons([...pokemons, ...pokemonsArray]);
  };

  return (
    <SafeAreaView>
      <PokemonList pokemons={pokemons} loadPokemons={loadPokemons} isNext={nextUrl} />
    </SafeAreaView>
  );
};

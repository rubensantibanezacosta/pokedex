import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { map, capitalize } from "lodash";
import { getColorByPokemonType } from "../../../../utils/pokemonColors";

export const Type: React.FC<any> = (props) => {
  const { types } = props;


  return (
    <View style={styles.content}>
      {map(types, (item, index) => (
        <View style={{...styles.pill, backgroundColor: getColorByPokemonType(item.type.name)}} key={index}>
            <Text>{capitalize(item.type.name)}</Text>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    marginTop: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
    pill: {
        paddingHorizontal: 30,
        paddingVertical: 5,
        borderRadius: 30,
        backgroundColor: "#fof",
        marginHorizontal: 10,
    }
});

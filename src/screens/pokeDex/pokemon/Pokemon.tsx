import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { PokemonClient } from "../../../clients/api/pokemonClient";
import { Header } from "./header/Header";
import { Type } from "./type/Type";
import { Stats } from "./stats/Stats";
import { Favourite } from "./favourite/Favourite";
import { useAuth } from "../../../hooks/useAuth";

export const Pokemon: React.FC<any> = (props) => {
  const {
    navigation,
    route: { params },
  } = props;

  const pokemonClient = PokemonClient.getInstance();
  const state: any = null;
  const [pokemon, setPokemon] = useState(state);
  const { auth } = useAuth();


  useEffect(() => {
    navigation.setOptions({
      headerRight: () => auth?<Favourite id={pokemon?.id}/>:null,
      headerLeft: () => <Icon name="arrow-left" size={20} color="#fff" style={{marginLeft: 8}} onPress={() => navigation.goBack()} />,

    });
  }, [navigation, params, pokemon]);

  useEffect(() => {
    (async () => {
      try {
        const res=await pokemonClient.getPokemonDetail(params.id);
        setPokemon(res);
      } catch (e) {
        navigation.goBack();
      }
    })();
  }, [params]);

  if (!pokemon) {

    return null;
  } else {
    return (
      <ScrollView>
        <Header
          name={pokemon.name}
          order={pokemon.order}
          image={pokemon.sprites.other["official-artwork"].front_default}
          type={pokemon.types[0].type.name}
        />
        <Type types={pokemon.types}/>
        <Stats stats={pokemon.stats}/>
      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({});

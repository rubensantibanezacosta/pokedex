import React, { useEffect, useState } from "react";
import EmptyIcon from "react-native-vector-icons/FontAwesome5";
import SolidIcon from "react-native-vector-icons/FontAwesome";

import {
  addToFavourites,
  getFavourites,
  removeFromFavourites,
} from "../../../../clients/local/localStorage";

export const Favourite: React.FC<any> = (props) => {
  const { id } = props;

  let favourite: any = null;

  const addFavourite = async () => {
    await addToFavourites(id);
    const favourites = await getFavourites();
    setFavouriteState(favourites.includes(id));
  };

  const removeFavourite = async () => {
    await removeFromFavourites(id);
    const favourites = await getFavourites();
    setFavouriteState(favourites.includes(id));
  };

  const [favouriteState, setFavouriteState] = useState(favourite);

  useEffect(() => {
    (async () => {
      const favourites = await getFavourites();

      setFavouriteState(favourites.includes(id));
    })();
  }, [id, addFavourite, favouriteState]);

  return (
    <>
      {favouriteState == true ? (
        <SolidIcon
          name="heart"
          size={20}
          color="white"
          onPress={removeFavourite}
          style={{ marginRight: 20 }}
        />
      ) : (
        <EmptyIcon
          name="heart"
          size={20}
          color="white"
          onPress={addFavourite}
          style={{ marginRight: 20 }}
        />
      )}
    </>
  );
};

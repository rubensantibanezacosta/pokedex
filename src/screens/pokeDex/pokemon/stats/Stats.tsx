import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { map, capitalize } from "lodash";

export const Stats: React.FC<any> = (props) => {
  const { stats } = props;

  const barStyles = (num: any) => {
    const color = num > 90 ? "red" : num > 70 ? "orange" : "green";
    return {
        backgroundColor: `${color}`,
        width: `${num}%`,
    }
  }

  return (
    <View style={styles.content}>
      <Text style={styles.title}>Base Stats</Text>
      {map(stats, (item, index) => (
        <View key={index} style={styles.block}>
          <View style={styles.blockTitle}>
            <Text style={styles.statName}>{capitalize(item.stat.name)}</Text>
          </View>
          <View style={styles.blockInfo}>
            <Text style={styles.number}>{item.base_stat}</Text>
            <View style={styles.bg}>
                <View style={[styles.bar, barStyles(item.base_stat)]}/>
            </View>
          </View>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: 20,
    marginTop: 40,
    marginBottom: 80,
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    paddingBottom: 10,
  },
  block: {
    flexDirection: "row",
    paddingVertical: 5,
  },
  blockTitle: {
    width: "35%",
  },
  statName: {
    fontSize: 12,
    color: "#6b6b6b",
  },
    blockInfo: {
        width: "65%",
        flexDirection: "row",
        alignItems: "center"
    },
    number: {
        width: "15%",
    },
    bg: {
        backgroundColor: "#dedede",
        width: "85%",
        height: 5,
        borderRadius: 20,
        overflow: "hidden",
    },
    bar: {
        height: 5,
        borderRadius: 20,
    }
});

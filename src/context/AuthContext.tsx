import React, { useState, createContext } from "react";

export const AuthContext = createContext({
    auth: null,
    login: (props:any) => {},
    logout: (props:any) => {},
});

export const AuthProvider = (props: any) => {
  const { children } = props;

  const [auth, setAuth] = useState(null);

    const login = (userData: any) => {
        setAuth(userData);
    }

    const logout = () => {
        setAuth(null);
    }

  const valueContext: any = {
    auth,
    login,
    logout,
  };

  return (
    <AuthContext.Provider value={valueContext}>{children}</AuthContext.Provider>
  );
};

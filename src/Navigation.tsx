import React from "react";
import Icon from "react-native-vector-icons/FontAwesome5";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Image } from "react-native";

import { AccountNavigation } from "./screens/account/AccountNavigation";
import { FavouriteNavigation } from "./screens/favourite/FavouriteNavigation";
import { PokeDexNavigation } from "./screens/pokeDex/PokedexNavigation";
//functional component

export const Navigation: React.FC = () => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator initialRouteName="PokeDex">
      <Tab.Screen
        name="Favourite"
        component={FavouriteNavigation}
        options={{
          tabBarLabel: "Favoritos",
          tabBarIcon: ({ color, size }) => {
            return <Icon name="heart" color={color} size={size} />;
          },
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="PokeDex"
        component={PokeDexNavigation}
        options={{
          tabBarLabel: "",
          tabBarIcon: renderPokeball,
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="Account"
        component={AccountNavigation}
        options={{
          tabBarLabel: "Mi cuenta",
          tabBarIcon: ({ color, size }) => {
            return <Icon name="user" color={color} size={size} />;
          },
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};

function renderPokeball() {
  return (
    <Image
      source={require("./assets/pokeball.png")}
      style={{
        width: 60,
        height: 60,
        top: -15,
        shadowColor: "#000",
        shadowOffset: {
          width: 2,
          height: 2,
        },
        shadowOpacity: 1,
        shadowRadius: 2,
      }}
    />
  );
}

import { API_HOST } from '../../config/config';


export class PokemonClient {


    private static instance: PokemonClient;
    private constructor() {
    }
    public static getInstance(): PokemonClient {
        if (!PokemonClient.instance) {
            PokemonClient.instance = new PokemonClient();
        }
        return PokemonClient.instance;
    }

    public getPokemons = async (endpointUrl:string) => {
        const url=`${API_HOST}/pokemon?limit=20%offset=0`;
        try {
            const response = await fetch(endpointUrl || url);
            const data = await response.json();
            return data;
        }
        catch (error) {
            throw error;
        }
    }

    public getPokemonDetailByUrl = async (url: string) => {
        try {
            const response = await fetch(url);
            const data = await response.json();
            return data;
        }
        catch (error) {
           throw error;
        }
    }

    public getPokemonDetail = async (id: number) => {
        const url=API_HOST+"/pokemon/"+id

        try {
            const response = await fetch(url);
            
            const data = await response.json();
            console.log(data);
            return data;
        }
        catch (error) {
            throw error;
        }
    }

}
import AsyncStorage from '@react-native-async-storage/async-storage';
import { includes, pull } from 'lodash';
import { FAVOURITE_STORAGE } from '../../utils/constants';

export const addToFavourites = async (id: number) => {
    try {
        const favourites = await getFavourites();
        if (!includes(favourites, id)) {
            await AsyncStorage.setItem(FAVOURITE_STORAGE, JSON.stringify([...favourites, id]));
        }
    } catch (error) {
        throw error;
    }
}

export const removeFromFavourites = async (id: number) => {
    try {
        const favourites = await getFavourites();
        if (includes(favourites, id)) {
            await AsyncStorage.setItem(FAVOURITE_STORAGE, JSON.stringify(pull(favourites, id)));
        }

    } catch (error) {
        throw error;
    }
}

export const getFavourites = async () => {
    try {
        const favourites: any = await AsyncStorage.getItem(FAVOURITE_STORAGE);
        return JSON.parse(favourites || '[]');
    } catch (error) {
        throw error;
    }

}
